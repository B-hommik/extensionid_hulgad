﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensionid_Hulgad
{
    class Inimene
    {
        public string Eesnimi;
        public string Perenimi;
        public int Vanus;
        public override string ToString()
        {
            return $"{Eesnimi} {Perenimi} on {Vanus} aastane";
        }
    }
}
